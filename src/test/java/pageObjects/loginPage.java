package pageObjects;


import utils.driverFactory;

public class loginPage extends driverFactory {

//locators
	public String singIn = "idcta-username"; // ID locator
	public String username = "user-identifier-input"; // ID locator
	public String password = "password-input"; // ID locator
	public String submit = "submit-button"; //ID locator
	public String invalidInput = "//p[@class='form-message__text']"; // xPath locator

// test data	
	public String invalidPasswordMessage = "Uh oh, that password doesn’t match that account. Please try again."; // expected message, used for validation
	public String noAccountMessage = "Sorry, we can’t find an account with that email. You can register for a new account or get help here."; //expected message, used for validation



}




